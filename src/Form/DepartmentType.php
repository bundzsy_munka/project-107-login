<?php

namespace App\Form;

use App\Entity\Department;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type as FormType;

class DepartmentType extends AbstractType
{
	public function buildForm (FormBuilderInterface $builder, array $options): void
	{

		if ($options['data']->getDeptNo()) {
			$builder
				->add('deptNo', FormType\IntegerType::class,
					[
						'label' => 'Department no',
						'required' => true,
						'disabled' => true,
					]);
		} else {
			$builder
				->add('deptNo', FormType\IntegerType::class,
					[
						'label' => 'Department no',
						'required' => true,
					]);
		}

		$builder
			->add('deptName')
			->add('save', FormType\SubmitType::class, [
				'label' => 'Save',
				'attr' => array('class' => 'btn btn-primary btn-sm')
			]);
	}

	public function configureOptions (OptionsResolver $resolver): void
	{
		$resolver->setDefaults([
			'data_class' => Department::class,
		]);
	}
}
