<?php

namespace App\DataFixtures;

use App\Entity\Department;
use App\Entity\DeptEmp;
use App\Entity\DeptManager;
use App\Entity\Employee;
use App\Entity\Salary;
use App\Entity\Title;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{

	private const EMPLOYEES = [
		[
			'empNo' => 1,
			'firstName' => 'Lengyel',
			'lastName' => 'András',
			'gender' => 'm',
			'birthDate' => '1985-09-30',
			'hireDate' => '2022-01-06',
		],
		[
			'empNo' => 2,
			'firstName' => 'Teszt',
			'lastName' => 'Elek',
			'gender' => 'w',
			'birthDate' => '1980-02-03',
			'hireDate' => '2021-01-13',
		]
	];

	private const TITLES = [
		[
			'title' => 'title 1',
			'fromDate' => '2021-04-04',
			'toDate' => '2021-04-09',
			'empNo' => 2
		],
		[
			'title' => 'title 2',
			'fromDate' => '2020-04-20',
			'toDate' => '2021-05-01',
			'empNo' => 2
		],
		[
			'title' => 'title 3',
			'fromDate' => '2018-10-10',
			'toDate' => '2020-02-10',
			'empNo' => 1
		],
	];

	private const SALARIES = [
		[
			'salary' => 400,
			'fromDate' => '2018-10-10',
			'toDate' => '2020-02-10',
			'empNo' => 1
		],
		[
			'salary' => 570,
			'fromDate' => '2020-02-10',
			'toDate' => '2023-01-01',
			'empNo' => 1
		],
		[
			'salary' => 380,
			'fromDate' => '2018-10-10',
			'toDate' => '2026-02-10',
			'empNo' => 2
		],
	];

	private const USERS = [
		[
			'username' => 'api-kacsa',
			'email' => 'api@api.com',
			'password' => 'kiskacsa1',
			'fullName' => 'Api Request',
			'roles' => [User::ROLE_ADMIN]
		],
		[
			'username' => 'teszt_elek',
			'email' => 'teszt_elek@teszt.com',
			'password' => 'qwertz2',
			'fullName' => 'Teszt Elek',
			'roles' => [User::ROLE_USER]
		],
		[
			'username' => 'admin',
			'email' => 'admin@admin.hu',
			'password' => 'qwertz',
			'fullName' => 'Super Admin',
			'roles' => [User::ROLE_ADMIN]
		],
		[
			'username' => 'user',
			'email' => 'user@user.hu',
			'password' => 'qwertz1',
			'fullName' => 'Teszt User',
			'roles' => [User::ROLE_USER]
		],
	];

	private const DEPARTMENTS = [
		[
			'deptNo' => '1',
			'deptName' => 'It',
		],
		[
			'deptNo' => '2',
			'deptName' => 'Marketing',
		],
		[
			'deptNo' => '3',
			'deptName' => 'Management',
		],
		[
			'deptNo' => '4',
			'deptName' => 'Sales',
		]
	];

	private const DEPT_EMPS = [
		[
			'empNo' => 1,
			'deptNo' => 1,
			'fromDate' => '2021-02-10',
			'toDate' => '2021-02-15',
		],
	];

	private const DEPT_MANAGER = [
		[
			'empNo' => 2,
			'deptNo' => 3,
			'fromDate' => '2021-02-10',
			'toDate' => '2021-02-15',
		],
	];

	/**
	 * @var UserPasswordEncoderInterface
	 */
	private $passwordEncoder;

	public function __construct (UserPasswordEncoderInterface $passwordEncoder)
	{
		$this->passwordEncoder = $passwordEncoder;
	}

	public function load (ObjectManager $manager): void
	{
		$this->loadUsers($manager);
		$this->loadEmployees($manager);
		$this->loadTitles($manager);
		$this->loadSalaries($manager);
		$this->loadDepartments($manager);
		$this->loadDeptEmps($manager);
		$this->loadDeptManager($manager);

		$manager->flush();
	}

	private function loadUsers (ObjectManager $manager): void
	{
		foreach (self::USERS as $userData) {
			$user = new User();
			$user->setUsername($userData['username']);
			$user->setFullName($userData['fullName']);
			$user->setEmail($userData['email']);
			$user->setPassword($this->passwordEncoder->encodePassword($user, $userData['password']));
			$user->setRoles($userData['roles']);

			$this->addReference($userData['username'], $user);

			$manager->persist($user);
		}
		$manager->flush();

	}

	private function loadEmployees (ObjectManager $manager)
	{
		foreach (self::EMPLOYEES as $employeeData) {
			$employee = new Employee();
			$employee->setBirthDate(new \DateTime($employeeData['birthDate']));
			$employee->setEmpNo($employeeData['empNo']);
			$employee->setFirstName($employeeData['firstName']);
			$employee->setLastName($employeeData['lastName']);
			$employee->setGender($employeeData['gender']);
			$employee->setHireDate(new \DateTime($employeeData['hireDate']));

			$manager->persist($employee);
		}
		$manager->flush();

	}


	private function loadTitles (ObjectManager $manager)
	{
		foreach (self::TITLES as $titleData) {
			$employee = $manager->getRepository(Employee::class)->findOneBy(['empNo' => $titleData['empNo']]);

			$title = new Title();
			$title->setEmpNo($employee);
			$title->setTitle($titleData['title']);
			$title->setFromDate(new \DateTime($titleData['fromDate']));
			$title->setToDate(new \DateTime($titleData['toDate']));

			$manager->persist($title);
		}
		$manager->flush();

	}

	private function loadSalaries (ObjectManager $manager)
	{
		foreach (self::SALARIES as $salaryData) {
			$employee = $manager->getRepository(Employee::class)->findOneBy(['empNo' => $salaryData['empNo']]);

			$salary = new Salary();
			$salary->setEmpNo($employee);
			$salary->setSalary($salaryData['salary']);
			$salary->setFromDate(new \DateTime($salaryData['fromDate']));
			$salary->setToDate(new \DateTime($salaryData['toDate']));

			$manager->persist($salary);
		}
		$manager->flush();

	}

	private function loadDepartments (ObjectManager $manager)
	{
		foreach (self::DEPARTMENTS as $departmentData) {
			$department = new Department();
			$department->setDeptNo($departmentData['deptNo']);
			$department->setDeptName($departmentData['deptName']);

			$manager->persist($department);
		}
		$manager->flush();
	}

	private function loadDeptEmps (ObjectManager $manager)
	{
		foreach (self::DEPT_EMPS as $deptEmpData) {
			$employee = $manager->getRepository(Employee::class)->findOneBy(['empNo' => $deptEmpData['empNo']]);
			$department = $manager->getRepository(Department::class)->findOneBy(['deptNo' => $deptEmpData['deptNo']]);

			$deptEmp = new DeptEmp();
			$deptEmp->setEmpNo($employee);
			$deptEmp->setDeptNo($department);
			$deptEmp->setFromDate(new \DateTime($deptEmpData['fromDate']));
			$deptEmp->setToDate(new \DateTime($deptEmpData['toDate']));

			$manager->persist($deptEmp);
		}
		$manager->flush();
	}

	private function loadDeptManager (ObjectManager $manager)
	{
		foreach (self::DEPT_EMPS as $deptManagerData) {
			$employee = $manager->getRepository(Employee::class)->findOneBy(['empNo' => $deptManagerData['empNo']]);
			$department = $manager->getRepository(Department::class)->findOneBy(['deptNo' => $deptManagerData['deptNo']]);

			$deptEmp = new DeptManager();
			$deptEmp->setEmpNo($employee);
			$deptEmp->setDeptNo($department);
			$deptEmp->setFromDate(new \DateTime($deptManagerData['fromDate']));
			$deptEmp->setToDate(new \DateTime($deptManagerData['toDate']));

			$manager->persist($deptEmp);
		}
		$manager->flush();
	}
}
