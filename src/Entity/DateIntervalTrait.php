<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

trait DateIntervalTrait
{
	/**
	 * @ORM\Column(name="from_date", type="date", nullable=true)
	 */
	protected \DateTimeInterface $fromDate;

	/**
	 * @ORM\Column(name="to_date", type="date", nullable=true)
	 */
	protected \DateTimeInterface $toDate;

	public function getFromDate (): ?\DateTimeInterface
	{
		return $this->fromDate;
	}

	public function setFromDate (\DateTimeInterface $fromDate): self
	{
		$this->fromDate = $fromDate;
		return $this;
	}

	public function getToDate (): ?\DateTimeInterface
	{
		return $this->toDate;
	}

	public function setToDate (\DateTimeInterface $toDate): self
	{
		$this->toDate = $toDate;
		return $this;
	}



}