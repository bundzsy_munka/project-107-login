<?php

namespace App\Entity;

use App\Repository\EmployeeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=EmployeeRepository::class)
 * @ORM\Table(name="employees")
 * @UniqueEntity(fields="empNo", message="This employee id is in use")
 */
class Employee
{
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer", name="emp_no")
     * @Assert\Length(min=1)
	 */
    private $empNo;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $birthDate;

    /**
     * @ORM\Column(type="string", length=14, nullable=true)
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=16, nullable=true)
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    private $gender;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $hireDate;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Title", mappedBy="empNo")
     */
    private $titles;

	/**
	 * @ORM\OneToMany(targetEntity="App\Entity\Salary", mappedBy="empNo")
	 */
	private $salaries;

	/**
	 * @ORM\OneToMany(targetEntity="App\Entity\DeptEmp", mappedBy="empNo")
	 */
	private $deptEmps;

	/**
	 * @ORM\OneToMany(targetEntity="App\Entity\DeptManager", mappedBy="empNo")
	 */
	private $deptManagers;

    public function __construct()
    {
        $this->titles = new ArrayCollection();
        $this->salaries = new ArrayCollection();
        $this->deptEmps = new ArrayCollection();
        $this->deptManagers = new ArrayCollection();
    }

    public function getEmpNo(): ?int
    {
        return $this->empNo;
    }

    public function setEmpNo(int $empNo): self
    {
        $this->empNo = $empNo;

        return $this;
    }

    public function getBirthDate(): ?\DateTimeInterface
    {
        return $this->birthDate;
    }

    public function setBirthDate(?\DateTimeInterface $birthDate): self
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(?string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getHireDate(): ?\DateTimeInterface
    {
        return $this->hireDate;
    }

    public function setHireDate(?\DateTimeInterface $hireDate): self
    {
        $this->hireDate = $hireDate;

        return $this;
    }

    /**
     * @return Collection|Title[]
     */
    public function getTitles(): Collection
    {
        return $this->titles;
    }

    public function addTitle(Title $title): self
    {
        if (!$this->titles->contains($title)) {
            $this->titles[] = $title;
            $title->setEmpNo($this);
        }

        return $this;
    }

    public function removeTitle(Title $title): self
    {
        if ($this->titles->removeElement($title)) {
            // set the owning side to null (unless already changed)
            if ($title->getEmpNo() === $this) {
                $title->setEmpNo(null);
            }
        }

        return $this;
    }

	/**
	 * @return Collection|Salary[]
	 */
	public function getSalaries(): Collection
	{
		return $this->salaries;
	}

	public function addSalary(Salary $salary): self
	{
		if (!$this->salaries->contains($salary)) {
			$this->salaries[] = $salary;
			$salary->setEmpNo($this);
		}

		return $this;
	}

	public function removeSalary(Salary $salary): self
	{
		if ($this->salaries->removeElement($salary)) {
			// set the owning side to null (unless already changed)
			if ($salary->getEmpNo() === $this) {
				$salary->setEmpNo(null);
			}
		}

		return $this;
	}

		public function getFullName()
	{
		return $this->getFirstName().' '.$this->getLastName();
	}

	/**
	 * @return Collection|DeptEmp[]
	 */
	public function getDeptEmps(): Collection
	{
		return $this->deptEmps;
	}

	public function addDeptEmp(DeptEmp $deptEmp): self
	{
		if (!$this->deptEmps->contains($deptEmp)) {
			$this->deptEmps[] = $deptEmp;
			$deptEmp->setEmpNo($this);
		}

		return $this;
	}

	public function removeDeptEmp(DeptEmp $deptEmp): self
	{
		if ($this->deptEmps->removeElement($deptEmp)) {
			// set the owning side to null (unless already changed)
			if ($deptEmp->getEmpNo() === $this) {
				$deptEmp->setEmpNo(null);
			}
		}

		return $this;
	}


	/**
	 * @return Collection|DeptManager[]
	 */
	public function getDeptManagers(): Collection
	{
		return $this->deptManagers;
	}

	public function addDeptManager(DeptManager $deptManager): self
	{
		if (!$this->deptManagers->contains($deptManager)) {
			$this->deptManagers[] = $deptManager;
			$deptManager->setEmpNo($this);
		}

		return $this;
	}

	public function removeDeptManager(DeptManager $deptManager): self
	{
		if ($this->deptManagers->removeElement($deptManager)) {
			// set the owning side to null (unless already changed)
			if ($deptManager->getEmpNo() === $this) {
				$deptManager->setEmpNo(null);
			}
		}

		return $this;
	}

}
