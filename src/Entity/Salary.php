<?php

namespace App\Entity;

use App\Repository\SalaryRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=SalaryRepository::class)
 * @ORM\Table(name="salaries")
 */
class Salary
{

	use DateIntervalTrait;

	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer", nullable=false)
	 */
	private $id;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\Employee", inversedBy="empNo")
	 * @ORM\JoinColumn(nullable=false, name="emp_no", referencedColumnName="emp_no", onDelete="CASCADE")
	 * @Assert\NotBlank()
	 */
	private $empNo;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     * @Assert\Length(min=1, max=99999999999)
     */
    private $salary;

	public function getId()
	{
		return $this->id;
	}

	public function getEmpNo(): ?employee
	{
		return $this->empNo;
	}

	public function setEmpNo(?employee $empNo): self
	{
		$this->empNo = $empNo;

		return $this;
	}

    public function getSalary(): ?int
    {
        return $this->salary;
    }

    public function setSalary(int $salary): self
    {
        $this->salary = $salary;

        return $this;
    }

	public function getFromDate (): ?\DateTimeInterface
	{
		return $this->fromDate;
	}

	public function setFromDate (\DateTimeInterface $fromDate): self
	{
		$this->fromDate = $fromDate;
		return $this;
	}

	public function getToDate (): ?\DateTimeInterface
	{
		return $this->toDate;
	}

	public function setToDate (\DateTimeInterface $toDate): self
	{
		$this->toDate = $toDate;
		return $this;
	}

}
