<?php

namespace App\Controller;

use App\Entity\DeptEmp;
use App\Form\DeptEmpType;
use App\Repository\DeptEmpRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Routing\RouterInterface;

/**
 * @Route("/dept_emp")
 */
class DeptEmpController extends AbstractController
{

	private EntityManagerInterface $em;
	private RouterInterface $router;
	private FlashBagInterface $flashBag;
	private FormFactoryInterface $formFactory;

	public function __construct(
		EntityManagerInterface $em,
		RouterInterface $router,
		FormFactoryInterface $formFactory,
		FlashBagInterface $flashBag)
	{
		$this->em = $em;
		$this->router = $router;
		$this->flashBag = $flashBag;
		$this->formFactory = $formFactory;
	}

    /**
     * @Route("/", name="dept_emp_list", methods={"GET"})
     * @Security("is_granted('ROLE_USER')", message="Access denied")
     */
    public function listAction(Request $request): Response
    {
	    /** @var DeptEmpRepository $deptEmpRepository */
	    $deptEmpRepository = $this->em->getRepository(DeptEmp::class);
	    $deptEmps = $deptEmpRepository->findAll();

	    return $this->render('crud/dept_emp/list.html.twig', [
		    'dept_emps' => $deptEmps,
	    ]);
    }

    /**
     * @Route("/create", name="dept_emp_create", methods={"GET", "POST"})
     * @Security("is_granted('ROLE_USER')", message="Access denied")
     */
    public function createAction(Request $request): Response
    {
        $deptEmp = new DeptEmp();
        $form = $this->formFactory->create(DeptEmpType::class, $deptEmp);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
	        $this->em->persist($deptEmp);
	        $this->em->flush();
	        $this->flashBag->add('notice', 'Dept-Emp created');

            return $this->redirectToRoute('dept_emp_list', [], Response::HTTP_SEE_OTHER);
        }

	    return new Response(
		    $this->render(
			    'crud/dept_emp/create.html.twig',
			    ['form' => $form->createView()]
		    )
	    );
    }

    /**
     * @Route("/{id}", name="dept_emp_show", methods={"GET"})
     * @Security("is_granted('ROLE_USER')", message="Access denied")
     */
    public function showAction(DeptEmp $deptEmp): Response
    {
        return $this->render('crud/dept_emp/show.html.twig', [
            'dept_emp' => $deptEmp,
        ]);
    }

    /**
     * @Route("/{id}/update", name="dept_emp_update", methods={"GET", "POST"})
     * @Security("is_granted('ROLE_USER')", message="Access denied")
     */
    public function updateAction(Request $request, DeptEmp $deptEmp): Response
    {
        $form = $this->formFactory->create(DeptEmpType::class, $deptEmp);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
	        $this->em->persist($deptEmp);
	        $this->em->flush();
	        $this->flashBag->add('notice', 'Dept-Emp updated');

            return $this->redirectToRoute('dept_emp_list', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('crud/dept_emp/update.html.twig', [
            'dept_emp' => $deptEmp,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/delete/{id}", name="dept_emp_delete")
     * @Security("is_granted('ROLE_USER')", message="Access denied")
     */
    public function deleteAction(DeptEmp $deptEmp): Response
    {
	    $this->em->remove($deptEmp);
	    $this->em->flush();

	    $this->flashBag->add('notice', 'Dept-emp was deleted');

	    return new RedirectResponse($this->router->generate('dept_emp_list'));
    }
}
