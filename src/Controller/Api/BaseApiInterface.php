<?php

namespace App\Controller\Api;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

interface BaseApiInterface
{

	public function getAction(Request $request): JsonResponse;
	public function putAction(Request $request): JsonResponse;
	public function postAction(Request $request): JsonResponse;
	public function deleteAction(Request $request): JsonResponse;


}