<?php

namespace App\Controller\Api;

use App\Controller\AbstractApiController;
use App\Entity\Department;
use App\Entity\DeptManager;
use App\Entity\Employee;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/api/dept_manager")
 */
class DeptManagerApiController extends AbstractApiController implements BaseApiInterface
{
	private EntityManagerInterface $em;

	private const ENTITY_TYPE = 'dept_manager';

	public function __construct (EntityManagerInterface $em, string $apiToken)
	{
		$this->setApiToken('Bearer ' . md5($apiToken));
		$this->setIsTokenValid(false);
		$this->em = $em;

	}

	/**
	 * @Route("/get", name="api_dept_manager_get", methods={"GET"})
	 */
	public function getAction (Request $request): JsonResponse
	{
		switch ($this->isTokenValid($request)) {
			case Response::HTTP_UNAUTHORIZED:
				return $this->error(['message' => 'error: authentication is not possible'], Response::HTTP_UNAUTHORIZED);
			case Response::HTTP_FORBIDDEN:
				return $this->error(['message' => 'error: authentication is not possible'], Response::HTTP_FORBIDDEN);
			case Response::HTTP_OK:
				$this->setIsTokenValid(true);
				break;
		}
		$repository = $this->em->getRepository(DeptManager::class);
		if ($request->get('id') !== null) {
			$entity = $repository->find($request->get('id'));
		} else {
			$entity = $repository->findAll();
		}

		if ($entity === null) {
			return $this->error(['message' => 'no matches with this id'], Response::HTTP_SERVICE_UNAVAILABLE);
		}

		$jsonData = $this->getDeptManagerJsonData($entity);
		return $this->success([
			'links' => 'http://localhost' . $request->getRequestUri(),
			'type' => self::ENTITY_TYPE,
			'method' => $request->getMethod(),
			'data' => $jsonData
		]);

	}

	/**
	 * @Route("/put", name="api_dept_manager_put", methods={"POST"})
	 */
	public function putAction (Request $request): JsonResponse
	{
		switch ($this->isTokenValid($request)) {
			case Response::HTTP_UNAUTHORIZED:
				return $this->error(['message' => 'error: authentication is not possible'], Response::HTTP_UNAUTHORIZED);
			case Response::HTTP_FORBIDDEN:
				return $this->error(['message' => 'error: authentication is not possible'], Response::HTTP_FORBIDDEN);
			case Response::HTTP_OK:
				$this->setIsTokenValid(true);
				break;
		}
		$formData = $request->get('form');
		$id = (int)$formData['id'];
		$entity = $this->em->getRepository(DeptManager::class)->findOneBy(['id' => $id]);
		if ($entity === null) {
			return $this->error(['message' => 'no matches with this id'], Response::HTTP_SERVICE_UNAVAILABLE);
		}

		try {
			$this->creatDeptManager($formData, $entity);
		} catch (\Exception $e) {
			return $this->error(['message' => $e->getMessage()], Response::HTTP_SERVICE_UNAVAILABLE);
		}
		return $this->success([
			'links' => 'http://localhost' . $request->getRequestUri(),
			'type' => self::ENTITY_TYPE,
			'method' => $request->getMethod(),
			'message' => 'Dept-Manager updated!'
		],
			Response::HTTP_CREATED);
	}

	/**
	 * @Route("/post", name="api_dept_manager_post", methods={"POST"})
	 */
	public function postAction (Request $request): JsonResponse
	{
		switch ($this->isTokenValid($request)) {
			case Response::HTTP_UNAUTHORIZED:
				return $this->error(['message' => 'error: authentication is not possible'], Response::HTTP_UNAUTHORIZED);
			case Response::HTTP_FORBIDDEN:
				return $this->error(['message' => 'error: authentication is not possible'], Response::HTTP_FORBIDDEN);
			case Response::HTTP_OK:
				$this->setIsTokenValid(true);
				break;
		}

		$formData = $request->get('form');

		try {
			$this->creatDeptManager($formData);
		} catch (\Exception $e) {
			return $this->error(['message' => $e->getMessage()], Response::HTTP_SERVICE_UNAVAILABLE);
		}
		return $this->success([
			'links' => 'http://localhost' . $request->getRequestUri(),
			'type' => self::ENTITY_TYPE,
			'method' => $request->getMethod(),
			'message' => 'Dept-Manager created!'
		],
			Response::HTTP_OK);

	}

	/**
	 * @Route("/delete", name="api_dept_manager_delete", methods={"GET"})
	 */
	public function deleteAction (Request $request): JsonResponse
	{
		switch ($this->isTokenValid($request)) {
			case Response::HTTP_UNAUTHORIZED:
				return $this->error(['message' => 'error: authentication is not possible'], Response::HTTP_UNAUTHORIZED);
			case Response::HTTP_FORBIDDEN:
				return $this->error(['message' => 'error: authentication is not possible'], Response::HTTP_FORBIDDEN);
			case Response::HTTP_OK:
				$this->setIsTokenValid(true);
				break;
		}
		$repository = $this->em->getRepository(DeptManager::class);

		try {
			$entity = $repository->find($request->get('id'));
			$this->em->remove($entity);
			$this->em->flush();
		} catch (\Exception $e) {
			return $this->error(['message' => $e->getMessage()], Response::HTTP_SERVICE_UNAVAILABLE);
		}

		$this->em->remove($entity);
		$this->em->flush();

		return $this->success([
			'links' => 'http://localhost' . $request->getRequestUri(),
			'type' => self::ENTITY_TYPE,
			'method' => $request->getMethod(),
			'message' => 'Dept-Manager deleted!'
		],
			Response::HTTP_OK);

	}

	private function getDeptManagerJsonData ($entity): array
	{
		$deptManagerJsonData = [];

		if ($entity instanceof DeptManager) {
			$deptManagerJsonData = $this->getDeptManagerData($entity);
		} else {
			foreach ($entity as $deptManager) {
				$deptManagerJsonData[] = $this->getDeptManagerData($deptManager);
			}
		}

		return $deptManagerJsonData;
	}

	private function getDeptManagerData (DeptManager $deptManager): array
	{
		return [
			'empNo' => $deptManager->getEmpNo()->getEmpNo(),
			'deptNo' => $deptManager->getDeptNo()->getDeptNo(),
			'fromDate' => $deptManager->getFromDate(),
			'toDate' => $deptManager->getToDate(),
		];
	}

	private function creatDeptManager ($formData, DeptManager $deptManager = null): void
	{
		if ($deptManager === null) {
			$deptManager = new DeptManager();
		}

		$department = $this->em->getRepository(Department::class)->findOneBy(['deptNo' => $formData['deptNo']]);
		$empNo = (int)$formData['empNo'];
		$employee = $this->em->getRepository(Employee::class)->findOneBy(['empNo' => $empNo]);

		$deptManager->setEmpNo($employee);
		$deptManager->setDeptNo($department);
		$deptManager->setFromDate((new \DateTime($formData['fromDate'])));
		$deptManager->setToDate((new \DateTime($formData['toDate'])));

		$this->em->persist($deptManager);
		$this->em->flush();
	}
}