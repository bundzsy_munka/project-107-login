<?php

namespace App\Controller\Api;

use App\Controller\AbstractApiController;
use App\Entity\Employee;
use App\Entity\Salary;
use App\Entity\Title;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/title")
 */
class TitleApiController extends AbstractApiController implements BaseApiInterface
{

	private EntityManagerInterface $em;

	private const ENTITY_TYPE = 'title';

	public function __construct (EntityManagerInterface $em, string $apiToken)
	{
		$this->setApiToken('Bearer ' . md5($apiToken));
		$this->setIsTokenValid(false);
		$this->em = $em;

	}

	/**
	 * @Route("/get", name="api_title_get", methods={"GET"})
	 */
	public function getAction (Request $request): JsonResponse
	{
		switch ($this->isTokenValid($request)) {
			case Response::HTTP_UNAUTHORIZED:
				return $this->error(['message' => 'error: authentication is not possible'], Response::HTTP_UNAUTHORIZED);
			case Response::HTTP_FORBIDDEN:
				return $this->error(['message' => 'error: authentication is not possible'], Response::HTTP_FORBIDDEN);
			case Response::HTTP_OK:
				$this->setIsTokenValid(true);
				break;
		}

		$repository = $this->em->getRepository(Title::class);
		if ($request->get('id') !== null) {
			$entity = $repository->find($request->get('id'));
		} else {
			$entity = $repository->findAll();
		}

		if ($entity === null) {
			return $this->error(['message' => 'no matches with this id'], Response::HTTP_SERVICE_UNAVAILABLE);
		}

		$jsonData = $this->getTitleJsonData($entity);
		return $this->success([
			'links' => 'http://localhost' . $request->getRequestUri(),
			'type' => self::ENTITY_TYPE,
			'method' => $request->getMethod(),
			'data' => $jsonData
		]);
	}

	/**
	 * @Route("/put", name="api_title_put", methods={"POST"})
	 */
	public function putAction (Request $request): JsonResponse
	{
		switch ($this->isTokenValid($request)) {
			case Response::HTTP_UNAUTHORIZED:
				return $this->error(['message' => 'error: authentication is not possible'], Response::HTTP_UNAUTHORIZED);
			case Response::HTTP_FORBIDDEN:
				return $this->error(['message' => 'error: authentication is not possible'], Response::HTTP_FORBIDDEN);
			case Response::HTTP_OK:
				$this->setIsTokenValid(true);
				break;
		}
		$formData = $request->get('form');
		$id = (int)$formData['id'];
		$entity = $this->em->getRepository(Title::class)->findOneBy(['id' => $id]);
		if ($entity === null) {
			return $this->error(['message' => 'no matches with this id'], Response::HTTP_SERVICE_UNAVAILABLE);
		}

		try {
			$this->creatTitle($formData, $entity);
		} catch (\Exception $e) {
			return $this->error(['message' => $e->getMessage()], Response::HTTP_SERVICE_UNAVAILABLE);
		}
		return $this->success([
			'links' => 'http://localhost' . $request->getRequestUri(),
			'type' => self::ENTITY_TYPE,
			'method' => $request->getMethod(),
			'message' => 'Title updated!'
		],
			Response::HTTP_CREATED);
	}

	/**
	 * @Route("/post", name="api_title_post", methods={"POST"})
	 */
	public function postAction (Request $request): JsonResponse
	{
		switch ($this->isTokenValid($request)) {
			case Response::HTTP_UNAUTHORIZED:
				return $this->error(['message' => 'error: authentication is not possible'], Response::HTTP_UNAUTHORIZED);
			case Response::HTTP_FORBIDDEN:
				return $this->error(['message' => 'error: authentication is not possible'], Response::HTTP_FORBIDDEN);
			case Response::HTTP_OK:
				$this->setIsTokenValid(true);
				break;
		}

		$formData = $request->get('form');

		try {
			$this->creatTitle($formData);
		} catch (\Exception $e) {
			return $this->error(['message' => $e->getMessage()], Response::HTTP_SERVICE_UNAVAILABLE);
		}
		return $this->success([
			'links' => 'http://localhost' . $request->getRequestUri(),
			'type' => self::ENTITY_TYPE,
			'method' => $request->getMethod(),
			'message' => 'Title created!'
		],
			Response::HTTP_OK);

	}


	/**
	 * @Route("/delete", name="api_title_delete", methods={"GET"})
	 */
	public function deleteAction (Request $request): JsonResponse
	{
		switch ($this->isTokenValid($request)) {
			case Response::HTTP_UNAUTHORIZED:
				return $this->error(['message' => 'error: authentication is not possible'], Response::HTTP_UNAUTHORIZED);
			case Response::HTTP_FORBIDDEN:
				return $this->error(['message' => 'error: authentication is not possible'], Response::HTTP_FORBIDDEN);
			case Response::HTTP_OK:
				$this->setIsTokenValid(true);
				break;
		}
		$repository = $this->em->getRepository(Title::class);

		try {
			$entity = $repository->find($request->get('id'));
			$this->em->remove($entity);
			$this->em->flush();
		} catch (\Exception $e) {
			return $this->error(['message' => $e->getMessage()], Response::HTTP_SERVICE_UNAVAILABLE);
		}

		$this->em->remove($entity);
		$this->em->flush();

		return $this->success([
			'links' => 'http://localhost' . $request->getRequestUri(),
			'type' => self::ENTITY_TYPE,
			'method' => $request->getMethod(),
			'message' => 'Title deleted!'
		],
			Response::HTTP_OK);

	}

	private function getTitleJsonData ($entity): array
	{
		$salaryJsonData = [];

		if ($entity instanceof Title) {
			$salaryJsonData = $this->getTitleData($entity);
		} else {
			foreach ($entity as $salary) {
				$salaryJsonData[] = $this->getTitleData($salary);
			}
		}

		return $salaryJsonData;
	}

	public function getTitleData (Title $title): array
	{
		return [
			'salary' => $title->getTitle(),
			'fromDate' => $title->getFromDate(),
			'toDate' => $title->getToDate(),
		];
	}

	private function creatTitle (array $formData, Title $title = null): void
	{
		if ($title === null) {
			$title = new Title();
		}

		$employee = $this->em->getRepository(Employee::class)->findOneBy(['empNo' => $formData['empNo']]);

		$title->setEmpNo($employee);
		$title->setFromDate((new \DateTime($formData['fromDate'])));
		$title->setToDate((new \DateTime($formData['toDate'])));
		$title->setTitle($formData['title']);

		$this->em->persist($title);
		$this->em->flush();
	}
}