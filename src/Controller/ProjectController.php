<?php


namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ProjectController extends AbstractController
{

	/**
	 * @var \Twig\Environment
	 */
	private $twig;

	public function __construct(
		\Twig\Environment $twig
	)
	{
		$this->twig = $twig;
	}

	/**
	 * @Route("/project", name="project_index")
	 */
	public function index()
	{
		return new Response(
			$this->twig->render('project/index.html.twig')
		);
	}


}