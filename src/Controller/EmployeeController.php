<?php

namespace App\Controller;

use App\Entity\Employee;
use App\Form\EmployeeType;
use App\Repository\EmployeeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Routing\RouterInterface;

/**
 * @Route("/employee")
 */
class EmployeeController extends AbstractCRUDController
{

	private EntityManagerInterface $em;
	private RouterInterface $router;
	private FlashBagInterface $flashBag;
	private FormFactoryInterface $formFactory;

	public function __construct (
		EntityManagerInterface $em,
		RouterInterface        $router,
		FormFactoryInterface $formFactory,
		FlashBagInterface      $flashBag)
	{
		$this->em = $em;
		$this->router = $router;
		$this->flashBag = $flashBag;
		$this->formFactory = $formFactory;
	}

	/**
	 * @Route("/", name="employee_list", methods={"GET"})
	 * @Security("is_granted('ROLE_USER')", message="Access denied")
	 */
	public function listAction (Request $request): Response
	{
		/** @var EmployeeRepository $employeeRepository */
		$employeeRepository = $this->em->getRepository(Employee::class);
		$employees = $employeeRepository->findAll();

		return $this->render('crud/employee/list.html.twig', [
			'employees' => $employees,
		]);
	}

	/**
	 * @Route("/create", name="employee_create", methods={"GET", "POST"})
	 * @Security("is_granted('ROLE_USER')", message="Access denied")
	 */
	public function createAction (Request $request): Response
	{
		$employee = new Employee();
		$form = $this->formFactory->create(EmployeeType::class, $employee);
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$this->em->persist($employee);
			$this->em->flush();
			$this->flashBag->add('notice', 'Employee created');

			return new RedirectResponse($this->router->generate('employee_list'));
		}

		return new Response(
			$this->render(
				'crud/employee/create.html.twig',
				['form' => $form->createView()]
			)
		);
	}

	/**
	 * @Route("/{empNo}", name="employee_show", methods={"GET"})
	 * @Security("is_granted('ROLE_USER')", message="Access denied")
	 */
	public function showAction (Employee $employee): Response
	{
		return $this->render('crud/employee/show.html.twig', [
			'employee' => $employee,
		]);
	}

	/**
	 * @Route("/{empNo}/update", name="employee_update", methods={"GET", "POST"})
	 * @Security("is_granted('ROLE_USER')", message="Access denied")
	 */
	public function updateAction(Request $request, Employee $employee): Response
	{
		$form = $this->formFactory->create(EmployeeType::class, $employee);
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$this->em->persist($employee);
			$this->em->flush();
			$this->flashBag->add('notice', 'Employee updated');

			return $this->redirectToRoute('employee_list', [], Response::HTTP_SEE_OTHER);

		}
		return $this->renderForm('crud/employee/update.html.twig', [
			'employee' => $employee,
			'form' => $form,
		]);
	}

	/**
	 * @Route("/delete/{empNo}", name="employee_delete")
	 * @Security("is_granted('ROLE_USER')", message="Access denied")
	 */
	public function deleteAction(Employee $employee): Response
	{
		$this->em->remove($employee);
		$this->em->flush();

		$this->flashBag->add('notice', 'Employee was deleted');

		return new RedirectResponse($this->router->generate('employee_list'));
	}
}
