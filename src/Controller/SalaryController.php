<?php

namespace App\Controller;

use App\Entity\Salary;
use App\Form\SalaryType;
use App\Repository\SalaryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Route("/salary")
 */
class SalaryController extends AbstractCRUDController
{
	private EntityManagerInterface $em;
	private RouterInterface $router;
	private FlashBagInterface $flashBag;
	private FormFactoryInterface $formFactory;

	public function __construct(
		EntityManagerInterface $em,
		RouterInterface $router,
		FormFactoryInterface $formFactory,
		FlashBagInterface $flashBag)
	{
		$this->em = $em;
		$this->router = $router;
		$this->flashBag = $flashBag;
		$this->formFactory = $formFactory;
	}

	/**
     * @Route("/", name="salary_list", methods={"GET"})
	 * @Security("is_granted('ROLE_USER')", message="Access denied")
     */
    public function listAction(Request $request): Response
    {
	    /** @var SalaryRepository $salaryRepository */
	    $salaryRepository = $this->em->getRepository(Salary::class);
	    $salaries = $salaryRepository->findAll();

	    return $this->render('crud/salary/list.html.twig', [
		    'salaries' => $salaries,
	    ]);
    }

    /**
     * @Route("/create", name="salary_create", methods={"GET", "POST"})
     * @Security("is_granted('ROLE_USER')", message="Access denied")
     */
    public function createAction(Request $request): Response
    {
        $salary = new Salary();
        $form = $this->formFactory->create(SalaryType::class, $salary);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($salary);
	        $this->em->flush();
	        $this->flashBag->add('notice', 'Salary created');

	        return new RedirectResponse($this->router->generate('salary_list'));
        }

	    return new Response(
		    $this->render(
			    'crud/salary/create.html.twig',
			    ['form' => $form->createView()]
		    )
	    );
    }

    /**
     * @Route("/{id}", name="salary_show", methods={"GET"})
     * @Security("is_granted('ROLE_USER')", message="Access denied")
     */
    public function showAction(Salary $salary): Response
    {
	    return $this->render('crud/salary/show.html.twig', [
		    'salary' => $salary,
	    ]);
    }

    /**
     * @Route("/{id}/update", name="salary_update", methods={"GET", "POST"})
     * @Security("is_granted('ROLE_USER')", message="Access denied")
     */
    public function updateAction(Request $request, Salary $salary): Response
    {
        $form = $this->formFactory->create(SalaryType::class, $salary);
        $form->handleRequest($request);

	    if ($form->isSubmitted() && $form->isValid()) {
		    $this->em->persist($salary);
		    $this->em->flush();
		    $this->flashBag->add('notice', 'Salary updated');

		    return $this->redirectToRoute('salary_list', [], Response::HTTP_SEE_OTHER);
	    }

	    return $this->renderForm('crud/salary/create.html.twig', [
		    'salary' => $salary,
		    'form' => $form,
	    ]);
    }

    /**
     * @Route("/delete/{id}", name="salary_delete")
     * @Security("is_granted('ROLE_USER')", message="Access denied")
     */
    public function deleteAction(Salary $salary): Response
    {
	    $this->em->remove($salary);
	    $this->em->flush();

	    $this->flashBag->add('notice', 'Salary was deleted');

	    return new RedirectResponse($this->router->generate('salary_list'));
    }
}
