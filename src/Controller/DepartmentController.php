<?php

namespace App\Controller;

use App\Entity\Department;
use App\Form\DepartmentType;
use App\Repository\DepartmentRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Route("/department")
 */
class DepartmentController extends AbstractCRUDController
{

	private EntityManagerInterface $em;
	private RouterInterface $router;
	private FlashBagInterface $flashBag;
	private FormFactoryInterface $formFactory;

	public function __construct (
		EntityManagerInterface $em,
		RouterInterface        $router,
		FormFactoryInterface $formFactory,
		FlashBagInterface      $flashBag)
	{
		$this->em = $em;
		$this->router = $router;
		$this->flashBag = $flashBag;
		$this->formFactory = $formFactory;
	}

	/**
	 * @Route("/", name="department_list", methods={"GET"})
	 * @Security("is_granted('ROLE_USER')", message="Access denied")
	 */
	public function listAction (Request $request): Response
    {
	    /** @var DepartmentRepository $departmentRepository */
	    $departmentRepository = $this->em->getRepository(Department::class);
	    $departments = $departmentRepository->findAll();

	    return $this->render('crud/department/list.html.twig', [
		    'departments' => $departments,
	    ]);
    }

	/**
	 * @Route("/create", name="department_create", methods={"GET", "POST"})
	 * @Security("is_granted('ROLE_USER')", message="Access denied")
	 */
	public function createAction (Request $request): Response
	{
		$department = new Department();
		$form = $this->formFactory->create(DepartmentType::class, $department);
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$this->em->persist($department);
			$this->em->flush();
			$this->flashBag->add('notice', 'Department created');

			return new RedirectResponse($this->router->generate('department_list'));
		}

		return new Response(
			$this->render(
				'crud/department/create.html.twig',
				['form' => $form->createView()]
			)
		);
	}


	/**
	 * @Route("/{deptNo}", name="department_show", methods={"GET"})
	 * @Security("is_granted('ROLE_USER')", message="Access denied")
	 */
	public function showAction (Department $department): Response
	{
		return $this->render('crud/department/show.html.twig', [
			'department' => $department,
		]);
	}

	/**
	 * @Route("/{deptNo}/update", name="department_update", methods={"GET", "POST"})
	 * @Security("is_granted('ROLE_USER')", message="Access denied")
	 */
	public function updateAction(Request $request, Department $department): Response
	{
		$form = $this->formFactory->create(DepartmentType::class, $department);
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$this->em->persist($department);
			$this->em->flush();
			$this->flashBag->add('notice', 'Department updated');

			return $this->redirectToRoute('department_list', [], Response::HTTP_SEE_OTHER);

		}
		return $this->renderForm('crud/department/update.html.twig', [
			'department' => $department,
			'form' => $form,
		]);
	}

    /**
     * @Route("/delete/{deptNo}", name="department_delete")
     * @Security("is_granted('ROLE_USER')", message="Access denied")
     */
	public function deleteAction(Department $department): Response
	{
		$this->em->remove($department);
		$this->em->flush();

		$this->flashBag->add('notice', 'Department was deleted');

		return new RedirectResponse($this->router->generate('department_list'));
	}
}
