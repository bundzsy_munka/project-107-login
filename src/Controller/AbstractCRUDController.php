<?php


namespace App\Controller;

use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

abstract class AbstractCRUDController extends AbstractApiController
{

	protected function isJsonRequest(Request $request): bool
	{
		if (!$request->headers->has('Accept') || false === mb_strpos($request->headers->get('Accept'), 'application/json')) {
			return false;
		}

		return true;
	}

	protected function jsonResponse(array $data, int $code = 200, array $headers = []): JsonResponse
	{
		$request = $this->get('request_stack')->getCurrentRequest();

		if (!isset($data['links'])) {
			$data['links'] = array();
		}
		$data['links'] = array_merge($data['links'], $this->getActions($request));

		return parent::jsonResponse($data, $code, $headers);
	}

	protected function getActions(Request $request): array
	{
		$routes = array();

		return $routes;
	}
}