<?php

namespace App\Controller;

use App\Entity\DeptManager;
use App\Form\DeptManagerType;
use App\Repository\DeptManagerRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Routing\RouterInterface;

/**
 * @Route("/dept_manager")
 */
class DeptManagerController extends AbstractController
{

	private EntityManagerInterface $em;
	private RouterInterface $router;
	private FlashBagInterface $flashBag;
	private FormFactoryInterface $formFactory;

	public function __construct(
		EntityManagerInterface $em,
		RouterInterface $router,
		FormFactoryInterface $formFactory,
		FlashBagInterface $flashBag)
	{
		$this->em = $em;
		$this->router = $router;
		$this->flashBag = $flashBag;
		$this->formFactory = $formFactory;
	}

	/**
	 * @Route("/", name="dept_manager_list", methods={"GET"})
	 * @Security("is_granted('ROLE_USER')", message="Access denied")
	 */
	public function listAction(Request $request): Response
	{
		/** @var DeptManagerRepository $deptManagerRepository */
		$deptManagerRepository = $this->em->getRepository(DeptManager::class);
		$deptManagers = $deptManagerRepository->findAll();

		return $this->render('crud/dept_manager/list.html.twig', [
			'dept_managers' => $deptManagers,
		]);
	}


	/**
	 * @Route("/create", name="dept_manager_create", methods={"GET", "POST"})
	 * @Security("is_granted('ROLE_USER')", message="Access denied")
	 */
	public function createAction(Request $request): Response
	{
		$deptManager = new DeptManager();
		$form = $this->formFactory->create(DeptManagerType::class, $deptManager);
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$this->em->persist($deptManager);
			$this->em->flush();
			$this->flashBag->add('notice', 'Dept-Manager created');

			return $this->redirectToRoute('dept_manager_list', [], Response::HTTP_SEE_OTHER);
		}

		return new Response(
			$this->render(
				'crud/dept_manager/create.html.twig',
				['form' => $form->createView()]
			)
		);
	}

	/**
	 * @Route("/{id}", name="dept_manager_show", methods={"GET"})
	 * @Security("is_granted('ROLE_USER')", message="Access denied")
	 */
	public function showAction(DeptManager $deptManager): Response
	{
		return $this->render('crud/dept_manager/show.html.twig', [
			'dept_manager' => $deptManager,
		]);
	}


	/**
	 * @Route("/{id}/update", name="dept_manager_update", methods={"GET", "POST"})
	 * @Security("is_granted('ROLE_USER')", message="Access denied")
	 */
	public function updateAction(Request $request, DeptManager $deptManager): Response
	{
		$form = $this->formFactory->create(DeptManagerType::class, $deptManager);
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$this->em->persist($deptManager);
			$this->em->flush();
			$this->flashBag->add('notice', 'Dept-Manager updated');

			return $this->redirectToRoute('dept_manager_list', [], Response::HTTP_SEE_OTHER);
		}

		return $this->renderForm('crud/dept_manager/update.html.twig', [
			'dept_manager' => $deptManager,
			'form' => $form,
		]);
	}

	/**
	 * @Route("/delete/{id}", name="dept_manager_delete")
	 * @Security("is_granted('ROLE_USER')", message="Access denied")
	 */
	public function deleteAction(DeptManager $deptManager): Response
	{
		$this->em->remove($deptManager);
		$this->em->flush();

		$this->flashBag->add('notice', 'Dept-manager was deleted');

		return new RedirectResponse($this->router->generate('dept_manager_list'));
	}
}
