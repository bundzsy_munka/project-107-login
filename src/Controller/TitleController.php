<?php

namespace App\Controller;

use App\Entity\Title;
use App\Form\TitleType;
use App\Repository\TitleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Routing\RouterInterface;


/**
 * @Route("/title")
 */
class TitleController extends AbstractCRUDController
{

	private EntityManagerInterface $em;
	private RouterInterface $router;
	private FlashBagInterface $flashBag;
	private FormFactoryInterface $formFactory;

	public function __construct(
		EntityManagerInterface $em,
		RouterInterface $router,
		FormFactoryInterface $formFactory,
		FlashBagInterface $flashBag)
	{
		$this->em = $em;
		$this->router = $router;
		$this->flashBag = $flashBag;
		$this->formFactory = $formFactory;
	}

    /**
     * @Route("/", name="title_list", methods={"GET"})
     * @Security("is_granted('ROLE_USER')", message="Access denied")
     */
    public function listAction(Request $request): Response
    {
	    /** @var TitleRepository $titleRepository */
	    $titleRepository = $this->em->getRepository(Title::class);
	    $titles = $titleRepository->findAll();

	    return $this->render('crud/title/list.html.twig', [
            'titles' => $titles,
        ]);
    }

    /**
     * @Route("/create", name="title_create", methods={"GET", "POST"})
     * @Security("is_granted('ROLE_USER')", message="Access denied")
     */
    public function createAction(Request $request): Response
    {
        $title = new Title();
        $form = $this->formFactory->create(TitleType::class, $title);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($title);
            $this->em->flush();
	        $this->flashBag->add('notice', 'Title created');

	        return new RedirectResponse($this->router->generate('title_list'));
        }

	    return new Response(
		    $this->render(
			    'crud/title/create.html.twig',
			    ['form' => $form->createView()]
		    )
	    );
    }

    /**
     * @Route("/{id}", name="title_show", methods={"GET"})
     * @Security("is_granted('ROLE_USER')", message="Access denied")
     */
    public function showAction(Title $title): Response
    {
        return $this->render('crud/title/show.html.twig', [
            'title' => $title,
        ]);
    }

    /**
     * @Route("/{id}/update", name="title_update", methods={"GET", "POST"})
     * @Security("is_granted('ROLE_USER')", message="Access denied")
     */
    public function updateAction(Request $request, Title $title): Response
    {
        $form = $this->formFactory->create(TitleType::class, $title);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
	        $this->em->persist($title);
	        $this->em->flush();
	        $this->flashBag->add('notice', 'Title updated');

            return $this->redirectToRoute('title_list', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('crud/title/update.html.twig', [
            'title' => $title,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/delete/{id}", name="title_delete")
     * @Security("is_granted('ROLE_USER')", message="Access denied")
     */
    public function deleteAction(Title $title): Response
    {
	    $this->em->remove($title);
	    $this->em->flush();

	    $this->flashBag->add('notice', 'Title was deleted');

	    return new RedirectResponse($this->router->generate('title_list'));
    }
}
