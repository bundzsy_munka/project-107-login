<?php

namespace App\Tests;

use App\Kernel;
use Doctrine\ORM\EntityManagerInterface;

trait TestHelperTrait
{
    /** @var EntityManagerInterface */
    private $em;


    public function init(){
        $kernel = new Kernel('dev', true);
        $kernel->boot();
        $this->em = $kernel->getContainer()->get('doctrine')->getManager();
    }

}