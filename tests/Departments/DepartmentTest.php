<?php

namespace App\Tests\Departments;

use App\Entity\Department;
use App\Tests\TestHelperTrait;
use PHPUnit\Framework\TestCase;

/**
 * Class DepartmentTest
 *
 * RUN command:
 * php vendor/bin/phpunit tests/Departments/DepartmentTest.php
 */
class DepartmentTest extends TestCase
{
    use TestHelperTrait;

    public const TEST_DEPARTMENT_DATA = [
        'deptNo' => 9999,
        'deptName' => 'test dept name'
    ];

    public function testDepartment (): void
    {
        $this->init();

        $this->creatDepartment(self::TEST_DEPARTMENT_DATA);
        $department = $this->em->getRepository(Department::class)->find(self::TEST_DEPARTMENT_DATA['deptNo']);
        $this->assertNotNull($department);

        $this->deleteDepartment(self::TEST_DEPARTMENT_DATA['deptNo']);
        $department = $this->em->getRepository(Department::class)->find(self::TEST_DEPARTMENT_DATA['deptNo']);
        $this->assertNull($department);
    }

    private function deleteDepartment(int $deptNo): void
    {
        $repository = $this->em->getRepository(Department::class);
        $entity = $repository->find($deptNo);
        $this->em->remove($entity);
        $this->em->flush();
    }


    private function creatDepartment (array $data): void
    {
        $department = new Department();
        $department->setDeptNo($data['deptNo']);
        $department->setDeptName($data['deptName']);

        $this->em->persist($department);
        $this->em->flush();
    }
}