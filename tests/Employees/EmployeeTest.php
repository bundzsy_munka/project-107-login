<?php

namespace App\Tests\Employees;

use App\Entity\Employee;
use App\Kernel;
use App\Tests\TestHelperTrait;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;

/**
 * Class EmployeeTest
 *
 * RUN command:
 * php vendor/bin/phpunit tests/Employee/EmployeeTest.php
 */
class EmployeeTest extends TestCase
{
    use TestHelperTrait;

    public const TEST_EMPLOYEE_DATA = [
        'empNo' => 9999,
        'hireDate' => '2022-01-10',
        'lastName' => 'András',
        'firstName' => 'Lengyel',
        'gender' => 'm',
        'birthDate' => '1985-09-30'
    ];


    public function testEmployee (): void
    {
        $this->init();

        $employee = $this->creatEmployee(self::TEST_EMPLOYEE_DATA);
        $this->assertNotNull($employee);

        $this->deleteEmployee(self::TEST_EMPLOYEE_DATA['empNo']);
        $employee = $this->em->getRepository(Employee::class)->find(self::TEST_EMPLOYEE_DATA['empNo']);
        $this->assertNull($employee);
    }

    private function creatEmployee(array $employeeData): ?Employee
    {
        if ($employeeData['empNo'] === null)
        {
            return null;
        }
        $employee = new Employee();
        $employee->setEmpNo($employeeData['empNo']);
        $employee->setHireDate((new \DateTime($employeeData['hireDate'])));
        $employee->setLastName($employeeData['lastName']);
        $employee->setFirstName($employeeData['firstName']);
        $employee->setGender($employeeData['gender']);
        $employee->setBirthDate((new \DateTime($employeeData['birthDate'])));

        $this->em->persist($employee);
        $this->em->flush();
        $this->em->clear();

        return $employee;
    }

    /**
     * @return Employee
     */
    private function deleteEmployee (int $empNo) :void
    {
        $repository = $this->em->getRepository(Employee::class);
        $entity = $repository->find($empNo);
        $this->em->remove($entity);
        $this->em->flush();
    }


}